function [ pivots v_disc ] = quantile_disc( values , nbin)
%QUANTILE_DISC Quantile discretization
%   values -- vector[N,1] of continuios value
%   nbin -- number of bins to discretaize
        
    if (size(values,1) == 1)
        error('size(values,1) == 1')
    end
     
     v_disc = ceil(nbin * tiedrank(values) / length(values));
     
     pivots = zeros(nbin-1,1);
     
     for i=1:nbin
         pivots(i) = max(values(v_disc == i));
     end
     
end