Welcome to DMI Repository
-------------------------------

1. GNU General Public License

This repo contains free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

--

2. Repository Description

This repo contains the scripts useful to execute the DMI method.
DMI (Differential Multi-Information) method is a computational strategy to
infer post-translational modulators of a transcription factor from a
compendium of Gene Expression Profiles (GEPs). DMI is built on the
hypothesis that a true modulator of a TF (i.e. kinase/phosphatases), when
expressed in the cell, will cause the TF target genes to be co-expressed.
On the contrary, when the modulator is not expressed, the TF will be
inactive resulting in a loss of co-regulation across its target genes. DMI
detects the occurrence of changes in target gene co-regulation for each
candidate modulator, using a measure called Multi-Information.