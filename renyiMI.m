function [rmi gamma Zn] = renyiMI( Xn, alpha, k, gamma, Zn)
%RENIYMI Estimation of Renyi multual information
%   INPUT
%       - Xn: NxD matrix of N i.i.d samples for D random variables.
%       - alpha: coefficient for the renyi mutual information
%       - k: numbers of neighbors to consider in the computation of Renyi MI
%
%   OUTPUT
%       - rmi: value of the estimed renyi mutual information
%
    

    %% determine # of sample n and the # of variables d and the power p
    [n d] = size(Xn);
    %p = d*(1-alpha);
    p = 1;
    
       
    %% Empirical copula transformation
    %disp('copula transformation...');
    
    if isempty(Zn)
        Zn = zeros(n,d);
        for j=1:d
            Zn(:,j) = empiricalCopula(Xn(:,j));
        end
    end
    
    clear Xn;
    
    %% compute g-knn
    %disp('Compute g-knn...');
    %Zn = genKNN(Zn,k);
    [~,Zn]=knnsearch(Zn,Zn,'K',k+1);
    %Zn(:,1) = [];
    
    %% Lp estimation
    %disp('Compute Lp of Zn');
    Lp = sumPpower(Zn,p);
    
    %% Empirical estimation of gamma
    %disp('estimation of gamma...')
    if isempty(gamma)
        gamma = computeGamma(10000,p,d,k+1);
    end
    %% reny MI estimation
    %rmi = - ( (1/(1-alpha)) * log(Lp/(gamma*n^(1-p/d))) );
    
    tmp = log(Lp) - (log(gamma) + (1-p/d)*log(n));
    
    rmi = - ( (1/(1-alpha)) *  tmp);
    
    %rmi = - ( (1/(1-alpha)) * log(Lp/gamma));
end
