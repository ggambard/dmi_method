function [ Z ] = empiricalCopula( X )
%EMPIRICALCOPULA empirical copula trsformation of e vector X
    
    %[X IX] = sort(X,'ascend');
    Z = zeros(length(X),1);
    
    for i=1:length(X)
        Z(i) = sum(X<=X(i))/length(X); 
    end

end

