function [ res ] = dmi_method( expression, map_platform, map_K, map_target, nbin)
%DMI_METHOD Summary of this function goes here
% DMI (Differential Multi-Information) method is a computational strategy
% to infer post-translational modulators of a transcription factor from a
% compendium of Gene Expression Profiles (GEPs). DMI is built on the
% hypothesis that a true modulator of a TF (i.e. kinase/phosphatases),
% when expressed in the cell, will cause the TF target genes to be
% co-expressed. On the contrary, when the modulator is not expressed, the
% TF will be inactive resulting in a loss of co-regulation across its
% target genes. DMI detects the occurrence of changes in target gene
% co-regulation for each candidate modulator, using a measure called
% Multi-Information.
%
%   INPUT:
%   expression      --> GEPs to use, a matrix genes x samples
%   map_platform    --> a vector containing the genes on the platform used
%   map_K           --> candidate modulators to test
%   map_target      --> target of the trascription factor of interest
%   nbin            --> # of bins in whch discretize the modulator expression
%   
%   
%   RESULTS FORMAT:
%        col. 1 --> gene symbol
%        col. 2 --> Fold Chage K (valid if we used RMA to normalize data)
%        col. 3 --> RMI with low K
%        col. 4 --> RMI with high K
%        col. 5 --> DELTA RMI
%


%% Build target and reglator expression
    
    
    Xn = expression(ismember(map_platform,map_target),:);
    
    idx_rgl = ismember(map_platform,map_K(:,1));    
    rgl_exp = expression(idx_rgl,:);
    map_rgl = map_platform(idx_rgl);
    
    %Reny parameters
    alpha = 0.99;
    k = 3;
    d = size(Xn,1)
    p = d*(1-alpha);
    disp('compute gamma...')
    gamma = computeGamma(50000,p,d,k+1);

    res = cell(length(map_rgl),5);
    
    clear expression;

	%% Delta Estimation
    for i=1:length(map_rgl);  
        
        % Probe (Modulator)
        res(i,1) = map_rgl(i);

        [ ~, v_disc ] = quantile_disc( rgl_exp(i,:)' , nbin);

		% RMI with Low K
        idx_bin = v_disc == 1;        
		rmi_dw = renyiMI( Xn(:,idx_bin)', alpha, k, gamma, []);
        if rmi_dw>0
            res(i,3) = {rmi_dw};
        else
            res(i,3) = {0};
        end
        k_dw = mean(rgl_exp(i,idx_bin)); % <-- change here if you are using MAS

		% RMI with High K
        idx_bin = v_disc == nbin;
        rmi_up = renyiMI( Xn(:,idx_bin)', alpha, k, gamma, []);
        if rmi_up>0
            res(i,4) = {rmi_up};
        else
            res(i,4) = {0};
        end
        k_up = mean(rgl_exp(i,idx_bin)); % <-- change here if you are using MAS
                  
			
		% Fold change Modulator
        res(i,2) = {k_up - k_dw}; % <-- change here if you are using MAS
		% Delta MI
        res(i,5) = {rmi_up - rmi_dw};

    end

    [~,ix] = sort(cell2mat(res(:,5)),'descend');
    res = res(ix,:);
end