function [ Lp ] = sumPpower( Xn, p )
%SUMPPOWER Compute Lp(Xn): the sum of the p-th powers of Euclidean lengths 
%of generalized kNN edges

    %elem = Xn(Xn>0);
    Lp = sum(sum(Xn.^p));
    
end

