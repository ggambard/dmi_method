function gamma = computeGamma( n, p, d, k )
%COMPUTEGAMMA Empirical estimation of lim Lp(X1:n)/n^(1-p/d)
%
% INPUT:
%   - n: # of i.i.d. sample to generate in order to compute gamma
%   - p: p-th powers of Euclidean lengths ( p = d*(1-alpha) )
%   - d: # of random variable to use ini the simulation
%   - k: # of nighbors to consider for each random variable
%
% OUTPUT:
%   - gamma: the empirical estimation of lim Lp(X1:n)/n^(1-p/d)
    

    %Generation of the i.i.d. sample from the uniform distribution over the
    %d-dimensional cube [0,1]^d
    
    disp('gaussian copula ...');
    SigmaInd = eye(d);
    Xn = mvnrnd(zeros(d,1), SigmaInd, n);
    Xn = normcdf(Xn,0,1);    
    Xn = icdf('unif',Xn,0,1);
    
    %Computes the generalized kNN graph    
    %Xn = genKNN(Xn,k);
    disp('knn ...');
    [~,Xn]=knnsearch(Xn,Xn,'K',k);
    %Xn = kNN(Xn,k);
    %Xn(:,1) = [];

    %Compute Lp(Xn): the sum of the p-th powers of Euclidean lengths of
    %generalized kNN edges
    disp('Lp sum...');
    Lp = sumPpower(Xn,p);
    
    %Empirical estimation of gamma
    gamma = Lp / n^(1-p/d);
    
    
end

